#ifndef AVLMAP_H
#define AVLMAP_H



#pragma once

#include <string>
#include <vector>

using namespace std;

struct node
{
	struct node* left;
	struct node* right;
	int key;
	int height;
	string value;

	node()
	{};

	node(int key, string value)
	{
		this->key = key;
		this->value = value;
		this->left = NULL;
		this->right = NULL;
		this->height = 0;
	}
};

class AVLMap
{
public:
	bool AVLMap::insert(int key, string value);
	bool AVLMap::remove(int key);
	bool AVLMap::find(int key, string& value);
	vector<string> AVLMap::findRange(int lowkey, int highkey);
	void AVLMap::print();
	void AVLMap::balanceCheck(int key);
	void AVLMap::setNull(node* node);
	void AVLMap::printHelper(node *node);
	int AVLMap::treeHeight(node* node);
	int AVLMap::getBalanceState(node* node);

private:
	int key;
	int nodeCounter = 0;
	int levelCounter = 0; 
	string value;
	
};

#endif