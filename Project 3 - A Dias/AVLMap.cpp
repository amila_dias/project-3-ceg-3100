#include "AVLMap.h"
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>

using namespace std;

int nodeCounter;
int levelCounter;
node *root;
node *parent;

int main()
{
	AVLMap newMap;

	newMap.insert(40, "12");
	newMap.insert(20, "16");
	newMap.insert(50, "11");
	newMap.insert(60, "test");
	newMap.insert(10, "54");
	newMap.insert(15, "15");
	newMap.insert(17, "17");
	newMap.insert(22, "22");
	newMap.insert(30, "16");
	newMap.insert(45, "72");

	newMap.print();


	cout << endl << endl;

	cout << newMap.treeHeight(root) << endl;


	system("pause");

	//REMOVE//
	//REMOVE//
	return 0;
	//REMOVE//
	//REMOVE//
}

//Insert completed, tested
bool AVLMap::insert(int key, string value)
{	
	node *newNode = new node(key, value);
	node *ptr;

	//check if root is there?
	//no root, create root
	if (nodeCounter == 0)
	{
		root = newNode;
		nodeCounter++;
		newNode->height = 0;
		cout << "node inserted at root, key: " << key << endl;
		ptr = root;
		return true;
	}	

	else if (nodeCounter > 0)
	{
		ptr = root;
		while (ptr != NULL)
		{
			if (ptr->key == key)
			{
				cout << "Node with key: " << key << " already exists" << endl;
				return false;
			}

			if (key < ptr->key)
			{
				parent = ptr;
				ptr = ptr->left;
			}
			else
			{
				parent = ptr;
				ptr = ptr->right;
			}
		}
		
		//create new node & make children null
		if (newNode->key < parent->key)
		{
			newNode->height = parent->height + 1;
			parent->left = newNode;
			cout << "Node inserted with Key: " << key << " with value - " << value << endl;
			nodeCounter++;
			//balanceCheck(key);
			return true;
		}
		else
		{	
			newNode->height = parent->height + 1;
			parent->right = newNode;
			cout << "Node inserted with Key: " << key << " with value - " << value << endl;
			nodeCounter++;
			//balanceCheck(key);
			return true;
		}

	}

	//perform check - is balanced?

	//recursive method for check
		//new method for checking
		//run balanceCheck with new int key

	cout << "Failure, no node inserted with KEY: " << key << endl;
	return false;
}

bool AVLMap::remove(int key)
{
	node *ptr = root;
	while (ptr != NULL)
	{
		if (parent->key == key)
		{
			parent = ptr;
		}

		if (key < ptr->key)
		{
			parent = ptr;
			ptr = ptr->left;
		}
		else
		{
			parent = ptr;
			ptr = ptr->right;
		}
	}

	if (parent->left == NULL && parent->right == NULL)
	{
		setNull(parent);
		nodeCounter--;
		cout << "Node at key " << key << " was deleted" << endl;
		return true;
	}
	else if (parent->left != NULL && parent->right == NULL || parent->right != NULL && parent->left == NULL)
	{
		if (parent->left != NULL)
		{
			parent = parent->left;
			setNull(parent->left);
			nodeCounter--;
		}
		else
		{
			parent = parent->right;
			setNull(parent->right);
			nodeCounter--;
		}
		return true;
	}
	else
	{
		//replace removed node with smallest value to the right of removed node

	}

	
	//use find function
		//if does not exist
			//throw error
		//if does exist
			//delete item
			//link items back


	//
	//
	//
	//
	//
	//
	//





	cout << "Node to be removed with key " << key << "was not found" << endl;
	return false;
}

//Find completed, tested
bool AVLMap::find(int key, string& value)
{
	//binary search

	//if found
	//value = value from found item
	//return true
	node *ptr = root;
	while (ptr != NULL)
	{
		if (ptr->key == key)
		{
			value = ptr->value;
			return true;
		}

		if (key < ptr->key)
		{
			parent = ptr;
			ptr = ptr->left;
		}
		else
		{
			parent = ptr;
			ptr = ptr->right;
		}
	}

		
	//if not found
		//return false
	cout << "value was not found" << endl;
	return false;
}

vector<string> AVLMap::findRange(int lowkey, int highkey)
{
	vector<string> range;
	
	//returns all items in tree between lowkey and highkey
	//find lowkey
		//in-order traverse tree until hightree
		//add item to vector with push_back until highkey
	//return vector even if empty

	//REMOVE//
	//REMOVE//
	return range;
	//REMOVE//
	//REMOVE//
}

//Print completed, tested
void AVLMap::print()
{
	printHelper(root);
	//right-child in order traversal
}

void AVLMap::printHelper(node *node)
{

	if (node == NULL)
	{
		return;
	}

	if (node->right != NULL)
	{
		printHelper(node->right);
	}

	for (int i = 0; i < node->height; i++)
	{
		cout << "\t";
	}
	cout << node->key << " , " << node->value << endl;

	if (node->left != NULL)
	{
		printHelper(node->left);
	}
}

void AVLMap::balanceCheck(int key)
{
	//using int key look at children
}

void AVLMap::setNull(node* node)
{
	node->key = NULL;
	node->value = "";
}

int AVLMap::treeHeight(node* node)
{
	if (node == NULL)
	{
		return -1;
	}
	
	int left = treeHeight(node->left);
	int right = treeHeight(node->right);

	if (left > right)
	{
		return left + 1;
	}
	else
	{
		return right + 1;
	}
}

int AVLMap::getBalanceState(node* node)
{
	int lHeight = treeHeight(node->left);
	int rHeight = treeHeight(node->right);

	return levelCounter;
}
